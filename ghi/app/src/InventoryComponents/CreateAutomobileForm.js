import React, { useEffect, useState } from "react";

function CreateAutomobile() {
	const [models, setModel] = useState([]);
	const [automobile, setAutomobile] = useState({
		color: "",
		year: "",
		vin: "",
		model_id: "",
	});

	const fetchModels = async () => {
		const modelUrl = "http://localhost:8100/api/models/";
		const response = await fetch(modelUrl);

		if (response.ok) {
			const data = await response.json();
			setModel(data.models);
		}
	};

	useEffect(() => {
		fetchModels();
	}, []);

	const handleSubmit = async (e) => {
		e.preventDefault();

		const autoUrl = "http://localhost:8100/api/automobiles/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(automobile),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(autoUrl, fetchConfig);

		if (response.ok) {
			setAutomobile({
				color: "",
				year: "",
				vin: "",
				model_id: "",
			});
		}
	};

	const handleFormChange = (e) => {
		const value = e.target.value;
		const inputName = e.target.name;
		setAutomobile({
			...automobile,

			[inputName]: value,
		});
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add an automobile to inventory</h1>
					<form onSubmit={handleSubmit} id="create-automobile-form">
						<div className="form-floating mb-3">
							<input
								value={automobile.color}
								onChange={handleFormChange}
								placeholder="Color"
								required
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={automobile.year}
								onChange={handleFormChange}
								placeholder="Year"
								required
								type="text"
								name="year"
								id="year"
								className="form-control"
							/>
							<label htmlFor="year">Year...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={automobile.vin}
								onChange={handleFormChange}
								placeholder="VIN"
								required
								type="text"
								name="vin"
								id="vin"
								className="form-control"
							/>
							<label htmlFor="vin">VIN...</label>
						</div>
						<div className="mb-3">
							<select
								value={automobile.model}
								onChange={handleFormChange}
								name="model_id"
								id="model_id"
								className="form-select"
								required
							>
								<option value="">Choose a model...</option>
								{models.map((model) => {
									return (
										<option value={model.id} key={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default CreateAutomobile;
