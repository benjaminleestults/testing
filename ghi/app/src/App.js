import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
// InventoryComponents
import AddManufacturer from "./InventoryComponents/AddManufacturerForm";
import CreateAutomobile from "./InventoryComponents/CreateAutomobileForm";
import CreateVehicleModel from "./InventoryComponents/CreateVehicleModel";
import ListAutomobiles from "./InventoryComponents/ListAutomobiles";
import ListManufacturers from "./InventoryComponents/ListManufacturers";
import ListVehicleModels from "./InventoryComponents/ListVehicleModels";
// ServiceComponents
import AddTechnician from "./ServiceComponents/AddTechnicianForm";
import CreateAppt from "./ServiceComponents/CreateApptForm";
import ListAppointments from "./ServiceComponents/ListAppointments";
import ListTechnicians from "./ServiceComponents/ListTechnicians";
import ServiceHistory from "./ServiceComponents/ServiceHistory";
// SalesComponents
import CustomerForm from "./SalesComponents/CustomerForm";
import CustomerList from "./SalesComponents/CustomerList";
import SalesForm from "./SalesComponents/SalesForm";
import SalesList from "./SalesComponents/SalesList";
import SalespersonForm from "./SalesComponents/SalespersonForm";
import SalespeopleList from "./SalesComponents/SalespeopleList";
import SalespersonHistory from "./SalesComponents/SalespersonHistory";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles/create" element={<CreateAutomobile />} />
          <Route path="/manufacturers/create" element={<AddManufacturer />} />
          <Route path="/appointments/create" element={<CreateAppt />} />
          <Route path="/appointments/" element={<ListAppointments />} />
          <Route path="/technicians/create" element={<AddTechnician />} />
          <Route path="/technicians" element={<ListTechnicians />} />
          <Route path="/appointments/history" element={<ServiceHistory />} />
          <Route path="/salespeople/create" element={<SalespersonForm/>} />
          <Route path="/salespeople" element={<SalespeopleList/>} />
          <Route path="/sales/create" element={<SalesForm/>} />
          <Route path="/sales" element={<SalesList/>} />
          <Route path="/customers/create" element={<CustomerForm/>} />
          <Route path="/customers" element={<CustomerList/>} />
          <Route path="/sales/history" element={<SalespersonHistory/>} />
          <Route path="/models" element={<ListVehicleModels/>} />
          <Route path="/models/create" element={<CreateVehicleModel/>} />
          <Route path="/manufacturer" element={<ListManufacturers/>} />
          <Route path="/automobiles" element={<ListAutomobiles/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
